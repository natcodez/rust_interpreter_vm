use vm::helpers::*;
use std::result::*;

pub fn structure(input: Vec<String>) -> Result<Vec<StructureInstructions>, String> {
    let mut iter = input.into_iter();    

    let mut chains: Vec<Vec<String>> = Vec::new();
    let mut output: Vec<StructureInstructions> = Vec::new();

    let mut current_chain: Vec<String> = Vec::new();
    let mut temp = String::new();

    while let Some(x) = iter.next() {        
        match x.as_ref() {

            "(" => {
                if temp != "" {
                    current_chain.push(temp.clone());
                    if temp != "(" && temp != "+" && temp != "-" && temp != "*" && temp != "/" {
                        chains.push(current_chain);
                        current_chain = Vec::new();
                    }
                    temp = String::new();
                }
                current_chain.push(x);
            }

            ")" => {
                if temp != "" {
                    current_chain.push(temp);
                }                
                temp = x.to_string();
            }

             "+" | "-" | "*" | "/" => {
                if temp != "" {
                    
                    current_chain.push(temp);
                    temp = String::new();
                }   

                if x != "-" {
                    if (current_chain[current_chain.len()-1] == "+") | (current_chain[current_chain.len()-1] == "-") 
                    | (current_chain[current_chain.len()-1] == "/") | (current_chain[current_chain.len()-1] == "*") {

                        return Err(format!("Error: {}{} is no valid operator", current_chain[current_chain.len()-1], x));
                    } 
                }

                current_chain.push(x);                
            }

            _ => {
                if temp == "" {
                    temp = x.to_string();
                }
                else {     
                    current_chain.push(temp);
                    chains.push(current_chain);
                    temp = x.to_string();
                    current_chain = Vec::new();
                }
            }
        }
    }

    if temp != "" {
        current_chain.push(temp);
        chains.push(current_chain);
    }


    let iter = chains.into_iter();

    for ch in iter {
        let mut c = 0;

        let mut operand_stack : Vec<StructureInstructions> = Vec::new();
        let mut operator_stack : Vec<StructureInstructions> = Vec::new();

        let res = recursive_struct(&mut operand_stack, &mut operator_stack, StructureInstructions::End, &mut c, &ch);

        match res {
            Ok(x) => {
                println!("found one: {}", x.destruct_print());   
                output.push(x);   
            }
            Err(x) => return Err(x),
        }
    }

    Ok(output) 
}

fn recursive_struct(operand_stack: &mut Vec<StructureInstructions>,
 operator_stack: &mut Vec<StructureInstructions>, last: StructureInstructions,
  pos: &mut usize, ar: &Vec<String>) -> Result<StructureInstructions,String> {

    if pos >= &mut ar.len() {
        while let Some(mut op) = operator_stack.pop() {

           if op.is_same_as(&StructureInstructions::Contain) {
               return Err("Error: Unclosed Paranthesis".to_owned());
           }             
           
           if let Some(exp1) =  operand_stack.pop() {
               if let Some(exp2) = operand_stack.pop() {
                   op = match match_instruction(op, exp1, exp2) {
                                Ok(x)  => x,
                                Err(x) => return Err(x),
                            };

                    if let Err(_x) = check_type_correctness(&op) {
                        return Err(format!("Type Error: Type1 != Type2 at {}: {}", op.to_string(), ar.join(" ")))
                    }

                   operand_stack.push(op);
                   continue;
               }
               return Err("Operand Stack Ended".to_owned())
           }
           return Err("Operand Stack Ended".to_owned())                         
        }          

        //Wrong unreachable expression Warning: Expression is reachable and Necessary
        return match operand_stack.pop() {
            Some(x) => return Ok(x), 
            None => return Err("Operand Ended early".to_owned())
        }   
    }

    match &(*ar[*pos]) {
        "(" => {
            operator_stack.push(StructureInstructions::Contain);
            let mut pos = &mut(*pos+1);
            return recursive_struct(operand_stack, operator_stack, StructureInstructions::Contain, pos, ar)
        } 

        ")" => {
            while let Some(mut op) = operator_stack.pop() {
                if op.is_same_as(&StructureInstructions::Contain) {
                    let mut pos = &mut(*pos+1);
                    return recursive_struct(operand_stack, operator_stack, StructureInstructions::Value(Type::UnEvaluated), pos, ar)
                }

                if let Some(exp1) =  operand_stack.pop() {
                    if let Some(exp2) = operand_stack.pop() {
                            
                        op = match match_instruction(op, exp1, exp2) {
                                Ok(x)  => x,
                                Err(x) => return Err(x),
                            };

                        if let Err(_x) = check_type_correctness(&op) {
                            return Err(format!("Type Error: Type1 != Type2 at {}: {}", op.to_string(), ar.join(" ")))
                        }

                        operand_stack.push(op);
                        
                        continue;
                    }
                return Err("Operand Stack Ended".to_owned())
               }
               return Err("Operand Stack Ended".to_owned())
      
            }
        }

        "*" | "/" | "+" | "-" => {
            let mut current = match &(*ar[*pos]) {
                "*" => StructureInstructions::Multiply(Box::new(StructureInstructions::End), Box::new(StructureInstructions::End)),
                "/" => StructureInstructions::Divide(Box::new(StructureInstructions::End), Box::new(StructureInstructions::End)),
                "-" => StructureInstructions::Subt(Box::new(StructureInstructions::End), Box::new(StructureInstructions::End)),
                "+" => StructureInstructions::Add(Box::new(StructureInstructions::End), Box::new(StructureInstructions::End)),
                _ => return Err("Impossible Error".to_owned())
            };

            if current.is_possible_unary()  {
                if last.is_operator() {
                    if current.is_same_as(&StructureInstructions::Subt(Box::new(StructureInstructions::End), Box::new(StructureInstructions::End))) {
                        operand_stack.push(StructureInstructions::Value(Type::NumberType(0f64)));
                        let mut unary = StructureInstructions::Unary(Box::new(current.clone()));

                        operator_stack.push(unary.clone());
                        let mut pos = &mut(*pos+1);
                        return recursive_struct(operand_stack, operator_stack, unary, pos, ar)
                    }
                }
            }          

            if let Some(mut op) = operator_stack.pop() {
                if current.is_bigger_than(&op) | &op.is_same_as(&StructureInstructions::Contain) {
                    operator_stack.push(op);
                } 
                else {
                    if let Some(exp1) =  operand_stack.pop() {
                        if let Some(exp2) = operand_stack.pop() {
                            
                            op = match match_instruction(op, exp1, exp2) {
                                Ok(x)  => x,
                                Err(x) => return Err(x),
                            };

                            if let Err(_x) = check_type_correctness(&op) {
                                return Err(format!("Type Error: Type1 != Type2 at {}: {}", op.to_string(), ar.join(" ")))
                            }

                            operand_stack.push(op.clone());
                            
                           return recursive_struct(operand_stack, operator_stack, StructureInstructions::Value(Type::UnEvaluated), pos, ar);                             
                            
                        }
                        return Err("Operand Stack Ended".to_owned())
                    }
                    return Err("Operand Stack Ended".to_owned())
                }                 
            }

            operator_stack.push(current.clone());
            let mut pos = &mut(*pos+1);
            return recursive_struct(operand_stack, operator_stack, current, pos, ar)
        }

        _ => {
            let mut temp = StructureInstructions::Value(find_type(&ar[*pos]));
            operand_stack.push(temp.clone());
            let mut pos = &mut(*pos + 1);
            return recursive_struct(operand_stack,operator_stack, temp, pos, ar)
        }
    };
    
    Ok(StructureInstructions::End)
}

fn match_instruction(op: StructureInstructions, exp1: StructureInstructions, exp2: StructureInstructions) -> Result<StructureInstructions, String> {
    match op {
                 StructureInstructions::Add(_x,_y)      => return Ok(StructureInstructions::Add(Box::new(exp1), Box::new(exp2))),
                 StructureInstructions::Subt(_x,_y)     => return Ok(StructureInstructions::Subt(Box::new(exp1), Box::new(exp2))),
                 StructureInstructions::Divide(_x,_y)   => return Ok(StructureInstructions::Divide(Box::new(exp1), Box::new(exp2))),
                 StructureInstructions::Multiply(_x,_y) => return Ok(StructureInstructions::Multiply(Box::new(exp1), Box::new(exp2))),
                 StructureInstructions::Unary(x)        =>   {
                                                                 match &*x {
                                                                     StructureInstructions::Subt(_z,_y) => return Ok(StructureInstructions::Subt(Box::new(exp1), Box::new(exp2))),
                                                                     _ => return Err(format!("Unknown Unary Operator: {}", x.to_string()))
                                                                 }                                                                                
                                                             }
                                                           
                 _ => return Err(format!("Unknown Operator: {}", op.to_string()))
             }
}

pub fn parse(input: &String) -> Vec<String> {
    let mut con = input.chars().peekable();

    let mut output = Vec::new();

    let mut s_temp = String::new();

    while let Some(&c) = con.peek() {
        match c {
            '0'...'9' | '_' | 'A'...'Z' | 'a'...'z' | '"' | '\'' | '.' => {
                s_temp += &c.to_string();
                con.next();
            }

            '+' | '*' | '-' | '/'| ',' | '(' | ')' => {
                if s_temp != "" {             
                    output.push(s_temp); 
                }
                output.push(c.to_string());
                s_temp = String::new();
                con.next();
            }          
            
            ' ' | _ => {
                if s_temp != "" {
                    if s_temp.chars().filter(|zz| zz == &'"').count() == 1 {
                        s_temp += &c.to_string();
                        con.next();
                        continue;
                    }
                    output.push(s_temp);
                }
                s_temp = String::new();
                con.next();                
            }            
        }
    }
    if s_temp != "" {
        output.push(s_temp);
    } 
    output   
}

fn check_type_correctness(i: &StructureInstructions) ->  Result<Type,String> {
      match i {
        StructureInstructions::Value(x) => return Ok(x.clone()),
        StructureInstructions::Add(x,y) => {
            match check_type_correctness(x) {
                Ok(t1) => {
                    match check_type_correctness(y){
                        Ok(t2) => {
                            if t1.is_same_as(&t2) {
                                return Ok(t1);
                            }
                            Err(format!("Type Error: Type1 != Type2 at Add: {} + {}", t2.to_string(), t1.to_string()))
                        }
                        Err(x) => Err(x)
                    }
                }
                Err(x) => Err(x)
            }  
        }

        StructureInstructions::Multiply(x,y) => {
            match check_type_correctness(x) {
                Ok(t1) => {
                    match check_type_correctness(y){
                        Ok(t2) => {
                            if t1.is_same_as(&t2) {
                                return Ok(t1);
                            }
                            Err(format!("Type Error: Type1 != Type2 at Mult: {} * {}", t2.to_string(), t1.to_string()))
                        }
                        Err(x) => Err(x),
                    }
                }
                Err(x) => Err(x),
            }  
        }

        StructureInstructions::Subt(x,y) => {
            match check_type_correctness(x) {
                Ok(t1) => {
                    match check_type_correctness(y){
                        Ok(t2) => {
                            if t1.is_same_as(&t2) {
                                return Ok(t1);
                            }
                            Err(format!("Type Error: Type1 != Type2 at Subt: {} * {}", t2.to_string(), t1.to_string()))
                        }
                        Err(x) => Err(x)
                    }
                }
                Err(x) => Err(x)
            }  
        }

        StructureInstructions::Divide(x,y) => {
            match check_type_correctness(x) {
                Ok(t1) => {
                    match check_type_correctness(y){
                        Ok(t2) => {
                            if t1.is_same_as(&t2) {
                                return Ok(t1);
                            }
                            Err(format!("Type Error: Type1 != Type2 at Divide: {} * {}", t2.to_string(), t1.to_string()))
                        }
                        Err(x) => Err(x)
                    }
                }
                Err(x) => Err(x)
            }  
        }

        _ => Err(format!("Unknown Type/ Instruction : {}", i.to_string()))
    }  
}

//Need to cach impossible operations