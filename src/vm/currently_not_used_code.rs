fn is_all_register(instr: &StructureInstructions) -> bool {
    match instr {
        StructureInstructions::Add(x,y)      => return is_all_register(x) && is_all_register(y),  
        StructureInstructions::Subt(x,y)     => return is_all_register(x) && is_all_register(y),
        StructureInstructions::Divide(x,y)   => return is_all_register(x) && is_all_register(y), 
        StructureInstructions::Multiply(x,y) => return is_all_register(x) && is_all_register(y),
        StructureInstructions::Value(x)      => return x.is_same_as(&Type::RegisterAdress(0)),
        
        _ => panic!("Unimplemented {} or Strange things happen", instr.to_string())
    }
}

fn set_register_adresses(instr: &mut StructureInstructions, const_address: u16, reg_adress: u8) {
    match instr {
        StructureInstructions::Add(x,y) => {
            set_register_adresses(x, const_address, reg_adress);
            set_register_adresses(y, const_address, reg_adress);            
        }
        StructureInstructions::Subt(x,y) => {
            set_register_adresses(x, const_address, reg_adress);
            set_register_adresses(y, const_address, reg_adress);
        }
        StructureInstructions::Divide(x,y) => {
            set_register_adresses(x, const_address, reg_adress);
            set_register_adresses(y, const_address, reg_adress);            
        }
        StructureInstructions::Multiply(x,y) => {
            set_register_adresses(x, const_address, reg_adress);
            set_register_adresses(y, const_address, reg_adress);
        }
        StructureInstructions::Value(x) => {
            if x.is_same_value_as(&Type::ConstReference(const_address)) {
                *x = Type::RegisterAdress(reg_adress);
            }                       
        }
        
        _=> {
            panic!("Unimplemented {} or Strange things happen", instr.to_string());
        }
    }
}





