use std::sync::MutexGuard;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::time::Duration;

#[derive(Clone)]
#[allow(dead_code)]
pub enum Instructions {
    EXIT      = 0x00,
    NOP       = 0x01,
    PUSH      = 0x02,
    POP       = 0x03,
    MOV       = 0x04,
    Load      = 0x05,
    LoadA     = 0x06,
    LOADO     = 0x07,
    LOADOA    = 0x08,   
    GOTO      = 0x09,
    
    Add       = 0x0A,
    Subt      = 0x0B,
    Multiply  = 0x0C,  
    Divide    = 0x0D,

    SETCONST  = 0x0E,
    INIT      = 0x0F,    
    DIE       = 0x10,
    CALL      = 0x11,
}


//252 GENERAL PURPOSE Registers
//0x00 - 0x2F for Constants
//0x30 - 0xFA for Objects? 
#[derive(Clone)]
#[allow(dead_code)]
pub enum Registers {    
    RE    = 0xfb,  //Return Value Register
    RI    = 0xfc,  //General Input Register
    RO    = 0xfd,  //General Output Register
    RP    = 0xfe,  //Print Register
    STACK = 0xff,
}

#[derive(Clone)]
#[allow(dead_code)]
pub enum TypeBytes {
    StringType      = 0x00,
    NumberType      = 0x01,
    BooleanType     = 0x02,   //0x00 = true, 0x01 = false
    UnEvaluated     = 0xff,    
}

#[derive(Clone)]
#[allow(dead_code)]
pub enum Type {
    StringType(String),
    NumberType(f64),
    BooleanType(bool),
    RegisterAdress(u8),
    ConstReference(u16),
    HeapReference(u64),
    Identifier(String),
    Object(Vec<Type>),    
    UnEvaluated,
}


#[derive(Clone)]
#[allow(dead_code)]
pub enum StructureInstructions {
    Add(Box<StructureInstructions>, Box<StructureInstructions>),
    Subt(Box<StructureInstructions>, Box<StructureInstructions>),
    Multiply(Box<StructureInstructions>, Box<StructureInstructions>),
    Divide(Box<StructureInstructions>, Box<StructureInstructions>),
    Unary(Box<StructureInstructions>),
    Set(String, Box<StructureInstructions>),    
    Contain,
    Value(Type),
    End,
}


impl Default for Type {
    fn default() -> Type {Type::UnEvaluated} 
}

impl Type {
    pub fn to_string(&self) -> String {
         match &self {
            Type::NumberType(x) => return x.to_string(),
            Type::StringType(x) => return x.to_string(),
            Type::BooleanType(x) => return x.to_string(),
            Type::RegisterAdress(x) => return x.to_string(),
            Type::ConstReference(x) => return x.to_string(),
            Type::HeapReference(x) => return x.to_string(),
            Type::Identifier(x) => return x.to_string(),
            Type::Object(x) => return x[0].to_string(),    //[0] = TypeName        
            Type::UnEvaluated => return String::from("Unknown"),
         }
    }

    pub fn to_byte_code(&self) -> Vec<u8> {
        let mut output: Vec<u8> = Vec::new();
        match &self {
            Type::BooleanType(x) => {
                output.push(TypeBytes::BooleanType as u8);
                output.push(match x {
                    true => 0x00,
                    false => 0x01
                });  
            }

            Type::NumberType(x) => {
                output.push(TypeBytes::NumberType as u8);
                let t = x.to_bits();
                let l_t = u64_to_array_of_u8(t);  
                for e in l_t {
                    output.push(e);                
                }
            }            

            Type::StringType(x) => {
                output.push(TypeBytes::StringType as u8);
                let t = x.as_bytes();
                let l = t.len() as u64;
                let l_t = u64_to_array_of_u8(l);  
                for e in l_t {
                    output.push(e);                
                }
                for e in t {
                    output.push(*e);
                }
            }

            Type::RegisterAdress(x) => {output.push(*x);}

            Type::ConstReference(_x) => {}

            Type::HeapReference(_x) => {}

            Type::Identifier(_x) => {}

            Type::Object(_x) => {}

            Type::UnEvaluated => {
                output.push(TypeBytes::UnEvaluated as u8);
            }
        }

        output
    }

    pub fn is_same_as(&self, to: &Type) -> bool {
        match &self {
            Type::UnEvaluated => { match to{                
                Type::UnEvaluated => return true,
                _ => return false,
            }}

            Type::BooleanType(_x) => { match to{                
                Type::BooleanType(_y) => return true,
                _ => return false,
            }}

            Type::NumberType(_x) => { match to{                
                Type::NumberType(_y) => return true,
                _ => return false,
            }}

            Type::StringType(_x) => { match to{                
                Type::StringType(_y) => return true,
                _ => return false,
            }}

            Type::RegisterAdress(_x) => { match to{                
                Type::RegisterAdress(_y) => return true,
                _ => return false,
            }} 

            Type::ConstReference(_x) => { match to{                
                Type::ConstReference(_y) => return true,
                _ => return false,
            }} 

            Type::HeapReference(_x) => { match to{                
                Type::HeapReference(_y) => return true,
                _ => return false,
            }}

            Type::Identifier(_x) => { match to{                
                Type::Identifier(_y) => return true,
                _ => return false,
            }}

            Type::Object(_x) => { match to{                
                Type::Identifier(_y) => return true,
                _ => return false,
            }}

        }
    } 

    pub fn is_same_value_as(&self, to: &Type) -> bool {
        match &self {
            Type::UnEvaluated => { match to{                
                Type::UnEvaluated => return true,
                _ => return false,
            }}

            Type::BooleanType(x) => { match to{                
                Type::BooleanType(y) => return x==y,
                _ => return false,
            }}

            Type::NumberType(x) => { match to{                
                Type::NumberType(y) => return x==y,
                _ => return false,
            }}

            Type::StringType(x) => { match to{                
                Type::StringType(y) => return x==y,
                _ => return false,
            }}

            Type::RegisterAdress(x) => { match to{                
                Type::RegisterAdress(y) => return x==y,
                _ => return false,
            }}

            Type::ConstReference(x) => { match to{                
                Type::ConstReference(y) => return x==y,
                _ => return false,
            }} 

            Type::HeapReference(x) => { match to{                
                Type::HeapReference(y) => return x==y,
                _ => return false,
            }} 

            Type::Identifier(x) => { match to{                
                Type::Identifier(y) => return x==y,
                _ => return false,
            }} 

            Type::Object(x) => { match to{                
                Type::Object(y) => {
                    if x.len() != y.len() {
                        return false;
                    }
                    let mut i1 = x.iter();
                    let mut i2 = y.iter();
                    while let (Some(o1),Some(o2)) = (i1.next(),i2.next()) {
                        if !o1.is_same_value_as(o2) {
                            return false;
                        }
                    }
                    return true
                    }
                _ => return false,
            }}           
        }
    }   
}

impl StructureInstructions {
    #[allow(dead_code)]
    pub fn to_byte_code(&self) -> Vec<u8> {
        match &self {
            StructureInstructions::Add(x,y) => {
                let mut prev_code = x.to_byte_code();
                prev_code.extend(y.to_byte_code());
                prev_code.push(Instructions::Add as u8);
                prev_code
            }
            StructureInstructions::Subt(x,y) => {
                let mut prev_code = x.to_byte_code();
                prev_code.extend(y.to_byte_code());
                prev_code.push(Instructions::Subt as u8);
                prev_code
            }
            StructureInstructions::Divide(x,y) => {
                let mut prev_code = x.to_byte_code();
                prev_code.extend(y.to_byte_code());
                prev_code.push(Instructions::Divide as u8);
                prev_code
            }
            StructureInstructions::Multiply(x,y) => {
                let mut prev_code = x.to_byte_code();
                prev_code.extend(y.to_byte_code());
                prev_code.push(Instructions::Multiply as u8);
                prev_code
            }
            StructureInstructions::Value(x) => {
                let mut prev_code = vec![Instructions::PUSH as u8];
                prev_code.extend(x.to_byte_code());
                prev_code
            }
            
            _=> {
                let s: Vec<u8> = Vec::new();
                return s
            }

        }
    } 

    pub fn to_string(&self) -> String {
         match &self {
            StructureInstructions::Add(_x,_y) => return String::from("Add"),
            StructureInstructions::Subt(_x,_y) => return String::from("Substract"),
            StructureInstructions::Multiply(_x,_y) => return String::from("Multiply"),
            StructureInstructions::Divide(_x,_y) => return String::from("Divide"),
            StructureInstructions::Unary(_x) => return String::from("Unary"),
            StructureInstructions::Set(_x,_y) => return String::from("Set"),
            StructureInstructions::Contain => return String::from("Contain"),
            StructureInstructions::Value(_x) => return String::from("Value"),
            StructureInstructions::End => return String::from("End"),
         }
    }

    pub fn destruct_print(&self) -> String {
        match &self {
            StructureInstructions::Add(x,y) => return format!("Add( {}, {} )",x.destruct_print() ,y.destruct_print()), 
            StructureInstructions::Subt(x,y) => return format!("Subt( {}, {} )",x.destruct_print() ,y.destruct_print()),
            StructureInstructions::Multiply(x,y) => return format!("Multiply( {}, {} )",x.destruct_print() ,y.destruct_print()),
            StructureInstructions::Divide(x,y) => return format!("Divide( {}, {} )",x.destruct_print() ,y.destruct_print()),
            StructureInstructions::Set(x,y) => return format!("Set {} to {} ",x ,y.destruct_print()),
            StructureInstructions::Unary(x) => return  x.destruct_print().to_string(),
            StructureInstructions::Contain => return String::from("Contain"),
            StructureInstructions::Value(x) => return x.to_string(),
            StructureInstructions::End => return String::from("End"),
        }
        
    }

    pub fn is_operator (&self) -> bool {
        match &self {            
            StructureInstructions::Value(_x) => return false,
            StructureInstructions::End => return false,
            _ => return true,
        }
    }

    pub fn is_possible_unary (&self) -> bool{
        match &self {
            StructureInstructions::Subt(_x,_y) => return true,
            _ => return false
        }    
    }

    pub fn is_same_as(&self, to: &StructureInstructions) -> bool {
        match &self {
            StructureInstructions::Add(_x,_y) => { match to{                
                StructureInstructions::Add(_xy, _xz) => return true,
                _ => return false,
            }}

            StructureInstructions::Subt(_x,_y) => { match to{                
                StructureInstructions::Subt(_xy, _xz) => return true,
                _ => return false,
            }}

            StructureInstructions::Multiply(_x,_y) => { match to{                
                StructureInstructions::Multiply(_xy, _xz) => return true,
                _ => return false,
            }}

            StructureInstructions::Divide(_x,_y) => { match to{                
                StructureInstructions::Divide(_xy, _xz) => return true,
                _ => return false,
            }}

            StructureInstructions::Unary(_x) => { match to{                
                StructureInstructions::Unary(_y) => return true,
                _ => return false,
            }}

            StructureInstructions::Set(_x,_y) => { match to{                
                StructureInstructions::Set(_xy, _xz) => return true,
                _ => return false,
            }}

            StructureInstructions::Contain => { match to{                
                StructureInstructions::Contain => return true,
                _ => return false,
            }}

            StructureInstructions::End => { match to{                
                StructureInstructions::End => return true,
                _ => return false,
            }}

            StructureInstructions::Value(_x) => { match to{                
                StructureInstructions::Value(_y) => return true,
                _ => return false,
            }}
        }
    }        

    pub fn is_bigger_than(&self, to: &StructureInstructions) -> bool {
        match &self {
            StructureInstructions::Add(_x,_y) => { match to{                
                StructureInstructions::Value(_xy)  => return true,
                StructureInstructions::End         => return true,
                StructureInstructions::Set(_y, _z) => return true,
                _ => return false,
            }}

            StructureInstructions::Subt(_x,_y) => { match to{                
                StructureInstructions::Value(_xy)  => return true,
                StructureInstructions::End         => return true,
                StructureInstructions::Set(_y, _z) => return true,
                _ => return false,
            }}

            StructureInstructions::Multiply(_x,_y) => { match to{
                StructureInstructions::Add(_xy, _xz)  => return true,
                StructureInstructions::Subt(_xy, _xz) => return true,                 
                StructureInstructions::Value(_xy)     => return true,
                StructureInstructions::End            => return true,
                StructureInstructions::Set(_y, _z)    => return true,
                _ => return false,
            }}

            StructureInstructions::Divide(_x,_y) => { match to{
                StructureInstructions::Add(_xy, _xz)  => return true,
                StructureInstructions::Subt(_xy, _xz) => return true,                 
                StructureInstructions::Value(_xy)     => return true,
                StructureInstructions::End            => return true,
                StructureInstructions::Set(_y, _z)    => return true,
                _ => return false,
            }}

            StructureInstructions::Unary(_x) => {match to {
                StructureInstructions::Contain   => return false,
                StructureInstructions::Unary(_y) => return false,
                _ => return true,
            }}

            StructureInstructions::Contain => { match to{                
                StructureInstructions::Contain => return false,
                _ => return true,
            }}

            StructureInstructions::Value(_x) => { match to{
                StructureInstructions::End         => return true,
                StructureInstructions::Set(_y, _z) => return true,
                _ => return false,
            }}

            StructureInstructions::Set(_x, _y) => { match to{
                StructureInstructions::End => return true,
                _ => return false,
            }}

            StructureInstructions::End => { match to{
                _ => return false,
            }}
        }
    }
}

pub fn u64_to_array_of_u8(x:u64) -> Vec<u8> {
    let b1 : u8 = ((x >> 56) & 0xff) as u8;
    let b2 : u8 = ((x >> 48) & 0xff) as u8;
    let b3 : u8 = ((x >> 40) & 0xff) as u8;
    let b4 : u8 = ((x >> 32) & 0xff) as u8;
    let b5 : u8 = ((x >> 24) & 0xff) as u8;
    let b6 : u8 = ((x >> 16) & 0xff) as u8;
    let b7 : u8 = ((x >> 8) & 0xff) as u8;
    let b8 : u8 = (x & 0xff) as u8;
    
    vec![b1, b2, b3, b4, b5, b6, b7, b8]
}

#[allow(dead_code)]
pub fn u32_to_array_of_u8(x:u32) -> Vec<u8> {
    let b1 : u8 = ((x >> 24) & 0xff) as u8;
    let b2 : u8 = ((x >> 16) & 0xff) as u8;
    let b3 : u8 = ((x >> 8) & 0xff) as u8;
    let b4 : u8 = (x & 0xff) as u8;
    
    vec![b1, b2, b3, b4]
}

pub fn u16_to_array_of_u8(x:u16) -> Vec<u8> {
    let b1 : u8 = ((x >> 8) & 0xff) as u8;
    let b2 : u8 = (x & 0xff) as u8;
    
    vec![b1, b2]
}



pub fn u8_array_to_u64(x: Vec<u8>) -> u64{
    ((x[0] as u64) << 56) |
    ((x[1] as u64) << 48) |
    ((x[2] as u64) << 40) |
    ((x[3] as u64) << 32) |
    ((x[4] as u64) << 24) |
    ((x[5] as u64) << 16) |
    ((x[6] as u64) <<  8) |
    (x[7] as u64)
}

#[allow(dead_code)]
pub fn u8_array_to_u32(x: Vec<u8>) -> u32{
    ((x[0] as u32) << 24) |
    ((x[1] as u32) << 16) |
    ((x[2] as u32) <<  8) |
    (x[3] as u32)
}

pub fn u8_array_to_u16(x: Vec<u8>) -> u16{
    ((x[0] as u16) <<  8) |
    (x[1] as u16)
}

pub static SLEEP: u64 = 100;

#[allow(dead_code)]
pub struct Wav<T> {
    pub value: Arc<Vec<Wux<T>>>,
}

#[allow(dead_code)]
impl<T> Wav<T>
    where T: Clone 
{
    
    pub fn new() -> Wav<T> {
        Wav{value: Arc::new(Vec::new())}
    }

    pub fn with_capacity(capacity: usize) -> Wav<T> {
        Wav{value: Arc::new(Vec::with_capacity(capacity))}
    }

    pub fn clone(&self) -> Wav<T> {
        Wav {value: Arc::clone(&self.value)}
    }

    pub fn push(&mut self, v: T) {
        Arc::get_mut(&mut self.value).unwrap().push(Wux::new(v));
    } 

    pub fn get (&self, index: usize) -> MutexGuard<T> {
        (*self.value)[index].get()
    }

    pub fn len(&self) -> usize {
        self.value.len()
    }

    pub fn contains(&self, index: usize) -> bool {
        (*self.value).len() > index
    }

    pub fn threadify (byte_code: &Wav<u8>, registers: &Wav<Type>, const_pool: &Wav<Type>, count: usize) {
        let b = byte_code.clone();
        let r = registers.clone();
        let cp = const_pool.clone();        

        thread::spawn(move || {
            let const_bytes = vec![*b.get(count+1), *b.get(count+2)];
            let register = *b.get(count+3) as usize;

            let const_adress =  u8_array_to_u16(const_bytes) as usize;
 
            *r.get(register) = (*cp.get(const_adress)).clone();
        });
    }  
}


#[allow(dead_code)]
pub struct Wux<T> {
    value: Mutex<T>,
}

#[allow(dead_code)]
impl<T> Wux<T> 
    where T: Clone   
{

    fn new(v: T) -> Wux<T>{
        Wux{value: Mutex::new(v)}
    }

    fn get(&self) -> MutexGuard<T>{
        loop {
            match self.value.lock(){
                Ok(x)   => return x,
                Err(_x) => thread::sleep(Duration::from_nanos(SLEEP)),
            }
        }
    } 
}

pub fn interprete_boolean(byte: u8) -> Result<Type, String> {
    match byte {
        0x00 => Ok(Type::BooleanType(true)),
        0x01 => Ok(Type::BooleanType(false)),
        _ => Err(String::from("Error Parsing boolean")),
    }
}

pub fn interprete_number(vec: Vec<u8>) -> Result<Type, String> {
    let bytes = u8_array_to_u64(vec);

    let flo = f64::from_bits(bytes);
    
    Ok(Type::NumberType(flo))
    
}

pub fn interprete_string(vec: Vec<u8>) -> Result<Type, String> {
    let inter = String::from_utf8(vec);
    match inter {
        Ok(x)  => Ok(Type::StringType(x)),
        Err(x) =>Err(format!("Number parsing error: {}", x))
    }
    
}

pub fn find_type(value: &str) -> Type {
    match value {
        "true"                                                 => Type::BooleanType(true),
        "false"                                                => Type::BooleanType(false),
        _ if value.parse::<f64>().is_ok()                      => Type::NumberType(value.parse::<f64>().unwrap()),
        _ if value.chars().filter(|z| z == &'"').count() == 2  => Type::StringType(value.chars().filter(|z| z != &'"').collect()),
        _ if value.chars().filter(|z| z == &'\'').count() == 2 => Type::StringType(value.chars().filter(|z| z != &'\'').collect()),
        _ => Type::StringType(value.to_owned()),           //TODO: Replace with var name                    
    }
}


pub fn match_registers_b(reg: String) -> u8 {    
    if reg == "RE" {
        return Registers::RE as u8;
    }

    if reg == "RI" {
        return Registers::RI as u8;
    }
 
    if reg == "RO" {
        return Registers::RO as u8;
    }

    if reg == "RP" {
        return Registers::RP as u8;
    }

    if reg == "STACK" {
        return Registers::STACK as u8;
    }

    let register = reg.replace("R", "").parse::<u8>();

    match register {
        Ok(x) => return x,
        Err(x) => panic!("{}", x)
    }     
}

pub fn match_registers(byte: u8) -> String {
    let mut output = String::new();
    if byte == 0xfb {
        output += "RE";
    }
    else if byte == 0xfc {
        output += "RI";
    }
    else if byte == 0xfd {
        output += "RO";
    }
    else if byte == 0xfe {
        output += "RP";
    }
    else if byte == 0xff {
        output += "STACK";
    }
    else {
        output += "R";
        output += &*byte.to_string();
    }

    output
}
