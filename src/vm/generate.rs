use std::path::Path;
use std::fs::*;
use std::fs;
use std::io::Read;
use std::io::BufWriter;
use std::io::Write;
use vm::helpers::*;
use std::result::*;
use vm::parse;
use vm::disassambler::*;

pub fn get_file_as_string(path: &Path) -> String {    
    println!("{}",path.display() );
    let mut f = File::open(path).expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    contents
} 

pub fn write_to_file(path: &Path, raw: Vec<u8>, disasambled: bool) -> Result<String, String> {
    let f = File::create(path);

    if disasambled {
        let st = disasamble(raw);               
        return match fs::write(path, st) {
            Ok(_res) => Ok(format!("finished compiling assembly file created at: \x1B[32m{}\x1B[0m", path.display())),
            Err(e)  => Err(format!("Write to File Error: {}", e)),
        }            
    }

    match f {
        Ok(file) => {
            let mut bw = BufWriter::new(file);
            return match bw.write_all(&raw[..]) {
                Ok(_res) => Ok(format!("finished compiling file created at: \x1B[32m{}\x1B[0m", path.display())),
                Err(e)  => Err(format!("Write to File Error: {}", e)),
            }
        }
        Err(x) => return Err(format!("couldn't create file: {} ; Reason: {}", path.display(), x)),
    }
}

pub fn compile(plain_text: &String, optimize: bool, from_asm: bool) -> Result<Vec<u8>,String> {

    if from_asm {
        return Ok(assemble(plain_text.clone()));
    } 

    let parsed_vec = parse::parse(&plain_text);

    let structured = parse::structure(parsed_vec);

    if let Ok(instr) = structured {        
         let finished_code = to_byte_code(instr, optimize);
        return Ok(finished_code);  
    }

    else if let Err(foo) = structured {
        println!("");
        return Err(foo)
    }

    let t = "";

    println!("{}",t);

    Err("Something went wrong".to_owned())
}





fn to_byte_code(mut instructions: Vec<StructureInstructions>, optimize: bool) -> Vec<u8> {
    let mut byte_code: Vec<u8> = Vec::new();
    let mut const_pool: Vec<Type> = Vec::new();
    let mut const_occurances: Vec<Type> = Vec::new();
    let mut registers: Vec<Type> = Vec::with_capacity(3);
    let mut register_use: Vec<(usize, u8)> = Vec::new();

    for mut instr in instructions.iter_mut() {
        if optimize {
            *instr = evaluate_const_optimization(instr);
        }        
        get_rec_consts(&mut instr, &mut const_pool, &mut const_occurances);
    }

    for (_i, constant) in const_pool.iter().enumerate() {
        byte_code.push(Instructions::SETCONST as u8);
        //byte_code.append(&mut u16_to_array_of_u8(i as u16));
        byte_code.append(&mut constant.to_byte_code())
    }

    for i in 0..252 { 
        if i >= const_pool.len() {
            break;
        }

        registers.push(Type::ConstReference(i as u16));

        byte_code.push(Instructions::Load as u8);
        byte_code.append(&mut u16_to_array_of_u8(i as u16));
        byte_code.push(i as u8);
    }

    for instr in instructions {
        let o = to_byte_code2(&instr, &mut byte_code, &mut registers, &mut register_use);        
        
        match o {
            Some(x) => {
                byte_code.push(Instructions::MOV as u8);                
                byte_code.push(x);
                register_use.push((byte_code.len(), x));
                
            }
            None => {
                byte_code.push(Instructions::POP as u8);
            }
        }
        byte_code.push(Registers::RP as u8);
    }

    byte_code.push(Instructions::EXIT as u8);

    return byte_code
}

fn get_rec_consts(instr: &mut StructureInstructions, const_pool: &mut Vec<Type>, const_occurances: &mut Vec<Type>) {
    match instr {
        StructureInstructions::Add(x,y) => {
            get_rec_consts(x, const_pool, const_occurances);
            get_rec_consts(y, const_pool, const_occurances);
        }
        StructureInstructions::Subt(x,y) => {
            get_rec_consts(x, const_pool, const_occurances);
            get_rec_consts(y, const_pool, const_occurances);
        }
        StructureInstructions::Divide(x,y) => {
            get_rec_consts(x, const_pool, const_occurances);
            get_rec_consts(y, const_pool, const_occurances);
        }
        StructureInstructions::Multiply(x,y) => {
            get_rec_consts(x, const_pool, const_occurances);
            get_rec_consts(y, const_pool, const_occurances);
        }
        StructureInstructions::Value(x) => {
            for (i, v) in const_pool.iter().enumerate() {
                if v.is_same_value_as(x) {
                    *x = Type::ConstReference(i as u16);
                    const_occurances.push(Type::ConstReference(i as u16));
                    return;
                }
            }
            let len = const_pool.len(); 
            const_pool.push(x.clone());
            *x = Type::ConstReference(len as u16);
            const_occurances.push(Type::ConstReference(len as u16));            
        }
        
        _=> {
            panic!("Unimplemented {} or Strange things happen", instr.to_string());
        }
    }
}


fn to_byte_code2(instr: &StructureInstructions, byte_code: &mut Vec<u8>, registers: &mut Vec<Type>, register_use: &mut Vec<(usize, u8)>) -> Option<u8> {
     match instr {
        StructureInstructions::Add(x,y) => {
            let o = to_byte_code2(x, byte_code, registers, register_use);
            let mut r1: u8;
            let mut r2: u8;
            let mut use1: Option<(usize, u8)> = None;
            let mut use2: Option<(usize, u8)> = None;


            match o {
                Some(x) => {
                    register_use.push((byte_code.len(), x));
                    use1 = Some((register_use.len()-1, x));
                    r1 = x;
                }
                None => {
                    r1 = Registers::STACK as u8;
                }
            }
            let o = to_byte_code2(y, byte_code, registers, register_use);
            
            match o {
                Some(x) => {    
                    register_use.push((byte_code.len(), x));
                    use2 = Some((register_use.len()-1, x));
                    r2 = x;
                }
                None => {
                    r2 = Registers::STACK as u8;
                }
            }            

            byte_code.push(Instructions::Add as u8);
            byte_code.push(r1);
            byte_code.push(r2);

            let mut rm = 0;

            if let Some(x) = use1 {
                let (i, y) = x;
                register_use.remove(i);
                register_use.push((byte_code.len()-1, y));
                rm = 1;
            }
            if let Some(x) = use2 {
                let (i, y) = x;
                register_use.remove(i-rm);
                register_use.push((byte_code.len()-1, y));
            }

            return None
        }
        StructureInstructions::Subt(x,y) => {
            let o = to_byte_code2(x, byte_code, registers, register_use);
            let mut r1: u8;
            let mut r2: u8;
            let mut use1: Option<(usize, u8)> = None;
            let mut use2: Option<(usize, u8)> = None;


            match o {
                Some(x) => {
                    register_use.push((byte_code.len()+3, x));
                    use1 = Some((register_use.len()-1, x));
                    r1 = x;
                }
                None => {
                    r1 = Registers::STACK as u8;
                }
            }
            let o = to_byte_code2(y, byte_code, registers, register_use);
            
            match o {
                Some(x) => {    
                    register_use.push((byte_code.len()+3, x));
                    use2 = Some((register_use.len()-1, x));
                    r2 = x;
                }
                None => {
                    r2 = Registers::STACK as u8;
                }
            }            

            byte_code.push(Instructions::Subt as u8);
            byte_code.push(r1);
            byte_code.push(r2);

            if let Some(x) = use1 {
                let (i, y) = x;
                register_use.remove(i);
                register_use.push((byte_code.len()-1, y))
            }
            if let Some(x) = use2 {
                let (i, y) = x;
                register_use.remove(i);
                register_use.push((byte_code.len()-1, y))
            }

            return None
        }
        StructureInstructions::Divide(x,y) => {
            let o = to_byte_code2(x, byte_code, registers, register_use);
            let mut r1: u8;
            let mut r2: u8;
            let mut use1: Option<(usize, u8)> = None;
            let mut use2: Option<(usize, u8)> = None;


            match o {
                Some(x) => {
                    register_use.push((byte_code.len()+3, x));
                    use1 = Some((register_use.len()-1, x));
                    r1 = x;
                }
                None => {
                    r1 = Registers::STACK as u8;
                }
            }
            let o = to_byte_code2(y, byte_code, registers, register_use);
            
            match o {
                Some(x) => {    
                    register_use.push((byte_code.len()+3, x));
                    use2 = Some((register_use.len()-1, x));
                    r2 = x;
                }
                None => {
                    r2 = Registers::STACK as u8;
                }
            }            

            byte_code.push(Instructions::Divide as u8);
            byte_code.push(r1);
            byte_code.push(r2);

            if let Some(x) = use1 {
                let (i, _y) = x;
                register_use.remove(i);
                register_use.push((byte_code.len()-1, r1))
            }
            if let Some(x) = use2 {
                let (i, _y) = x;
                register_use.remove(i);
                register_use.push((byte_code.len()-1, r2))
            }

            return None
        }
        StructureInstructions::Multiply(x,y) => {
            let o = to_byte_code2(x, byte_code, registers, register_use);
            let mut r1: u8;
            let mut r2: u8;
            let mut use1: Option<(usize, u8)> = None;
            let mut use2: Option<(usize, u8)> = None;


            match o {
                Some(x) => {
                    register_use.push((byte_code.len()+3, x));
                    use1 = Some((register_use.len()-1, x));
                    r1 = x;
                }
                None => {
                    r1 = Registers::STACK as u8;
                }
            }
            let o = to_byte_code2(y, byte_code, registers, register_use);
            
            match o {
                Some(x) => {    
                    register_use.push((byte_code.len()+3, x));
                    use2 = Some((register_use.len()-1, x));
                    r2 = x;
                }
                None => {
                    r2 = Registers::STACK as u8;
                }
            }            

            byte_code.push(Instructions::Multiply as u8);
            byte_code.push(r1);
            byte_code.push(r2);

            if let Some(x) = use1 {
                let (i, y) = x;
                register_use.remove(i);
                register_use.push((byte_code.len()-1, y))
            }
            if let Some(x) = use2 {
                let (i, y) = x;
                register_use.remove(i);
                register_use.push((byte_code.len()-1, y))
            }

            return None
        }
        StructureInstructions::Value(x) => {
            for reg in 0..registers.len() {
                if registers[reg].is_same_value_as(&x) {                    
                    return Some(reg as u8)
                }
            }
            let mut count: Vec<u8> = Vec::new();
            let mut _byte = 0;
            let mut iter = register_use.iter().rev();
            while let Some(xx) = iter.next() {
                let (xy, xz) = xx;
                if let Some(_z) = count.iter().find(|y| y == &xz) {
                    continue;
                }
                count.push(*xz);
                _byte = *xy;
                if count.len() == registers.len() {
                    break;
                }
            }
            

            _byte += 1;
            
            if let Type::ConstReference(y) = x {
                 
                 //********************************************\\
                //Need for refactor if Async would be introduced\\
               //************************************************\\

            /*    byte_code.insert(byte, count[count.len()-1]);
                let v = u16_to_array_of_u8(*y);
                byte_code.insert(byte, v[1]);
                byte_code.insert(byte, v[0]);
                byte_code.insert(byte, Instructions::LoadA as u8);   */
                let v = u16_to_array_of_u8(*y);
                byte_code.push(Instructions::Load as u8);
                byte_code.push(v[0]);
                byte_code.push(v[1]);
                byte_code.push(count[count.len()-1]);
                registers[count[count.len()-1] as usize] = Type::ConstReference(*y);
                return Some(count[count.len()-1])
            } 

            return None;
        }

        _ => panic!("Unimplemented {} or Strange things happen", instr.to_string())
    }    
}

fn evaluate_const_optimization(instr: &mut StructureInstructions) -> StructureInstructions {
     match instr.clone() {
        StructureInstructions::Add(mut x, mut y) => {
            x = Box::new(evaluate_const_optimization(&mut x.clone()));
            y = Box::new(evaluate_const_optimization(&mut y.clone()));

            if let StructureInstructions::Value(ref t1) = *x.clone() {
                if let StructureInstructions::Value(ref t2) = *y.clone() {
                    match (t2, t1) {
                        (Type::NumberType(n1), Type::NumberType(n2)) => *instr = StructureInstructions::Value(Type::NumberType(n1+n2)),                        

                        (Type::StringType(s1), Type::StringType(s2)) => *instr = StructureInstructions::Value(Type::StringType(s1.clone() + s2)),

                        (Type::BooleanType(b1), Type::BooleanType(b2)) => *instr = StructureInstructions::Value(Type::BooleanType(b1^b2)),

                        _ => panic!("Impossible, cached earlyier!")
                    }
                } else {*instr = StructureInstructions::Add(x.clone(), y.clone())}
            } else {*instr = StructureInstructions::Add(x.clone(), y.clone())}

            return instr.clone();            
        }
        StructureInstructions::Subt(mut x,mut y) => {
            x = Box::new(evaluate_const_optimization(&mut x.clone()));
            y = Box::new(evaluate_const_optimization(&mut y.clone()));

            if let StructureInstructions::Value(ref t1) = *x {
                if let StructureInstructions::Value(ref t2) = *y {
                    match (t2, t1) {
                        (Type::NumberType(n1), Type::NumberType(n2)) => *instr = StructureInstructions::Value(Type::NumberType(n1-n2)),

                        _ => panic!("Impossible, cached earlyier!")
                    }
                } else {*instr = StructureInstructions::Subt(x.clone(), y.clone())}
            } else {*instr = StructureInstructions::Subt(x.clone(), y.clone())}

            return instr.clone();  
        }
        StructureInstructions::Divide(mut x, mut y) => {
            x = Box::new(evaluate_const_optimization(&mut x.clone()));
            y = Box::new(evaluate_const_optimization(&mut y.clone()));

            if let StructureInstructions::Value(ref t1) = *x {
                if let StructureInstructions::Value(ref t2) = *y {
                    match (t2, t1) {
                        (Type::NumberType(n1), Type::NumberType(n2)) => *instr = StructureInstructions::Value(Type::NumberType(n1/n2)),                        

                        _ => panic!("Impossible, cached earlyier!")
                    }
                } else {*instr = StructureInstructions::Divide(x.clone(), y.clone())}
            } else {*instr = StructureInstructions::Divide(x.clone(), y.clone())}

            return instr.clone();      
        }
        StructureInstructions::Multiply(mut x, mut y) => {
            x = Box::new(evaluate_const_optimization(&mut x.clone()));
            y = Box::new(evaluate_const_optimization(&mut y.clone()));

            if let StructureInstructions::Value(ref t1) = *x {
                if let StructureInstructions::Value(ref t2) = *y {
                    match (t2, t1) {
                        (Type::NumberType(n1), Type::NumberType(n2)) => *instr = StructureInstructions::Value(Type::NumberType(n1*n2)),

                        (Type::BooleanType(b1), Type::BooleanType(b2)) => *instr = StructureInstructions::Value(Type::BooleanType(*b1&&*b2)),

                        _ => panic!("Impossible, cached earlyier!")
                    }
                } else {*instr = StructureInstructions::Multiply(x.clone(), y.clone())}
            } else {*instr = StructureInstructions::Multiply(x.clone(), y.clone())}

            return instr.clone();  
        }
        StructureInstructions::Value(_x) => { 
            return instr.clone();                
        }
        
        _=> {
            panic!("Unimplemented {} or Strange things happen", instr.to_string());
        }
    }
}

