use vm::helpers::*;

pub fn disasamble(byte_code: Vec<u8>) -> String {
    let mut output = String::new();
    let mut count = 0;
    let mut const_count = 0;
    while count < byte_code.len(){
        let inst = byte_code[count];

        match inst {
            inst if inst == Instructions::EXIT as u8 => {
                output += "EXIT";
                output += "\n";
            }

            inst if inst == Instructions::NOP as u8 => {
                output += "NOP";
                output += "\n";
            }

            inst if inst == Instructions::PUSH as u8 => {
                panic!("PUSH not implemented");
            }

            inst if inst == Instructions::POP as u8 => {
                output += "POP ";
                count += 1;

                output += &*match_registers(byte_code[count]);
                
                output += "\n";
            }

            inst if inst == Instructions::MOV as u8 => {
                output += "MOV ";

                count += 1;
                output += &*match_registers(byte_code[count]);
                output += ", ";
                
                count += 1;
                output += &*match_registers(byte_code[count]);

                output += "\n";
            }

            inst if inst == Instructions::Load as u8 => {
                output += "LOAD ";

                count = count +1;
                let mut bytes = vec![byte_code[count],  byte_code[count+1]];
                count = count +1;
                count = count +1;
                let register = match_registers(byte_code[count]);

                let const_adress =  u8_array_to_u16(bytes) as usize;

                output += "const.";
                output += &*const_adress.to_string();
                output += ", ";
                output += &*register;
                output += "\n";
            }

            inst if inst == Instructions::LoadA as u8 => {
                output += "LOADA ";

                count = count +1;
                let mut bytes = vec![byte_code[count],  byte_code[count+1]];
                count = count +1;
                count = count +1;
                let register = match_registers(byte_code[count]);

                let const_adress =  u8_array_to_u16(bytes) as usize;

                output += "const.";
                output += &*const_adress.to_string();
                output += ", ";
                output += &*register;
                output += "\n";
            }

            inst if inst == Instructions::LOADO as u8 => {
                panic!("LOADO not implemented");
            }

            inst if inst == Instructions::LOADOA as u8 => {
                panic!("LOADOA not implemented");
            }

            inst if inst == Instructions::GOTO as u8 => {
                panic!("GOTO not implemented");
            }

            inst if inst == Instructions::Add as u8 => {
                output += "ADD ";

                count += 1;
                output += &*match_registers(byte_code[count]);

                output += ", ";
                
                count += 1;
                output += &*match_registers(byte_code[count]);

                output += "\n";
            }

            inst if inst == Instructions::Subt as u8 => {
                output += "SUBT ";

                count += 1;
                output += &*match_registers(byte_code[count]);

                output += ", ";
                
                count += 1;
                output += &*match_registers(byte_code[count]);

                output += "\n";
            }

            inst if inst == Instructions::Multiply as u8 => {
                output += "MULT ";

                count += 1;
                output += &*match_registers(byte_code[count]);

                output += ", ";
                
                count += 1;
                output += &*match_registers(byte_code[count]);

                output += "\n";
            }

             inst if inst == Instructions::Divide as u8 => {
                output += "DIV ";

                count += 1;
                output += &*match_registers(byte_code[count]);

                output += ", ";
                
                count += 1;
                output += &*match_registers(byte_code[count]);
                
                output += "\n";
            }

             inst if inst == Instructions::SETCONST as u8 => {
                output += "SETCONST ";

                count = count +1;
                let typ = byte_code[count];
                
                match typ {
                    typ if typ == TypeBytes::BooleanType as u8 => {

                        count = count +1;
                        match interprete_boolean(byte_code[count]) {
                            Ok(x) => {if let Type::BooleanType(b) = x {
                                output += &*b.to_string();
                            };}
                            Err(x)  => panic!("Error Parsing Boolean: {}", x),
                        }
                    }

                    typ if typ == TypeBytes::NumberType as u8 => {
                        
                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(byte_code[count]);
                        }                       

                        match interprete_number(u8_vec) {
                            Ok(x) => {if let Type::NumberType(b) = x {
                                output += &*b.to_string();
                            };}
                            Err(x)  => panic!("Error Parsing Number: {}", x),
                        }
                    }

                    typ if typ == TypeBytes::StringType as u8 => {

                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(byte_code[count]);
                        }
                        let bytes = u8_array_to_u64(u8_vec);

                        u8_vec= Vec::new();

                        for _i in 0..bytes {
                            count = count +1;
                            u8_vec.push(byte_code[count]);
                        }

                        match interprete_string(u8_vec) {
                            Ok(x) => {if let Type::StringType(b) = x {
                                output += "\"";
                                output += &*b;
                                output += "\"";
                            };}
                            Err(x)  => panic!("Error Parsing tring: {}", x),
                        }
                    }

                    typ if typ == TypeBytes::UnEvaluated as u8 => {

                        panic!("Error: Unevaluated Type: {}", typ);
                    }

                    _ => panic!("Error: Unknown Type specifier: {}", typ)
                }

                output += " #const.";
                output += &*const_count.to_string();
                const_count += 1;
                
                output += "\n";
            }

            inst if inst == Instructions::INIT as u8 => {
                panic!("INIT is not implemented");
            }            

            inst if inst == Instructions::DIE as u8 => {
                panic!("DIE is not implemented");
            }

            inst if inst == Instructions::CALL as u8 => {
                panic!("CALL is not implemented");
            }

            _=> {panic!("Unexpected tocken or: {} is not implemented", byte_code[count]);}
        }

        count += 1;
    }

    output
}

pub fn assemble(assembly: String) -> Vec<u8> {
    let mut output = Vec::new();

    for line in assembly.lines() {
        let instr: Vec<&str> = line.splitn(2, ' ').collect();
        if instr.len() < 2 {
            if instr.len() < 1 {
                continue;
            }
            
            match instr[0] {
                "EXIT" => output.push(Instructions::EXIT as u8),
                "NOP" => output.push(Instructions::NOP as u8),
                _ => continue,
            }
            continue;            
        }
        match instr[0] {
            "LOAD" => {
                output.push(Instructions::Load as u8);
                let params: Vec<&str> = instr[1].splitn(3, ", ").collect();

                if params.len() < 2 {
                    panic!("Not correctly formated: {}", line);
                }
                let constant = params[0].replace("const.", "").parse::<u16>();

                match constant {
                    Ok(x) => output.append(&mut u16_to_array_of_u8(x)),
                    Err(x) => panic!("{}", x)
                }
                
                let reg = match_registers_b(params[1].to_string());

                output.push(reg);  
            }

            "LOADA" => {
                output.push(Instructions::LoadA as u8);
                let params: Vec<&str> = instr[1].splitn(3, ", ").collect();

                if params.len() < 2 {
                    panic!("Not correctly formated: {}", line);
                }
                let constant = params[0].replace("const.", "").parse::<u16>();

                match constant {
                    Ok(x) => output.append(&mut u16_to_array_of_u8(x)),
                    Err(x) => panic!("{}", x)
                }
                
                let reg = match_registers_b(params[1].to_string());

                output.push(reg);  
            }

            "ADD" => {
                
                output.push(Instructions::Add as u8);
                let params: Vec<&str> = instr[1].splitn(3, ", ").collect();

                if params.len() < 2 {
                    panic!("Not correctly formated: {}", line);
                }
                                
                let reg = match_registers_b(params[0].to_string());
                output.push(reg);

                let reg = match_registers_b(params[1].to_string());
                output.push(reg);            
            }

            "SUBT" => {
                
                output.push(Instructions::Subt as u8);
                let params: Vec<&str> = instr[1].splitn(3, ", ").collect();

                if params.len() < 2 {
                    panic!("Not correctly formated: {}", line);
                }
                                
                let reg = match_registers_b(params[0].to_string());
                output.push(reg);

                let reg = match_registers_b(params[1].to_string());
                output.push(reg);            
            }

            "MULT" => {
                
                output.push(Instructions::Multiply as u8);
                let params: Vec<&str> = instr[1].splitn(3, ", ").collect();

                if params.len() < 2 {
                    panic!("Not correctly formated: {}", line);
                }
                                
                let reg = match_registers_b(params[0].to_string());
                output.push(reg);

                let reg = match_registers_b(params[1].to_string());
                output.push(reg);            
            }

            "DIV" => {
                
                output.push(Instructions::Divide as u8);
                let params: Vec<&str> = instr[1].splitn(3, ", ").collect();

                if params.len() < 2 {
                    panic!("Not correctly formated: {}", line);
                }
                                
                let reg = match_registers_b(params[0].to_string());
                output.push(reg);

                let reg = match_registers_b(params[1].to_string());
                output.push(reg);            
            }

            "POP" => {                
                output.push(Instructions::POP as u8);
                let params: Vec<&str> = instr[1].splitn(2, ' ').collect();

                if params.len() < 1 {
                    panic!("Not correctly formated: {}", line);
                }
                                
                let reg = match_registers_b(params[0].to_string());
                output.push(reg);          
            }

            "SETCONST" => {
                output.push(Instructions::SETCONST as u8);
                let params: Vec<&str> = instr[1].splitn(2, '#')
                                                .map(|x| x.trim())
                                                .collect();

                let t = find_type(params[0]);

                output.append(&mut t.to_byte_code());
            }            

            _ => panic!("Not implemented: {}", line),
        }
    }

    output
}