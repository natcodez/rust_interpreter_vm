
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use std::env::current_dir;
    use self::test::Bencher;
    use vm::generate;     
    use vm::runtime;   

    #[bench]
    fn bench_optimized_compile_time(b: &mut Bencher) {
        // Optionally include some setup
        let mut buf = current_dir().unwrap();

        buf.push("test2.txt");
        let b2 = buf.clone();
        let filename = b2.as_path();

        let plain_text = generate::get_file_as_string(filename);

        

        b.iter(|| {
            let _res = generate::compile(&plain_text, true, false);
        });
    }

    #[bench]
    fn bench_unoptimized_compile_time(b: &mut Bencher) {
        // Optionally include some setup
        let mut buf = current_dir().unwrap();

        buf.push("test2.txt");
        let b2 = buf.clone();
        let filename = b2.as_path();

        let plain_text = generate::get_file_as_string(filename);        

        b.iter(|| {
            let _res = generate::compile(&plain_text, false, false);
        });
    }

    #[bench]
    fn bench_optimized_runtime(b: &mut Bencher) {
        // Optionally include some setup
        let mut buf = current_dir().unwrap();

        buf.push("test2.txt");
        let b2 = buf.clone();
        let filename = b2.as_path();

        let plain_text = generate::get_file_as_string(filename);
        let res = generate::compile(&plain_text, true, false);

        if let Ok(x) = res {
            let k = x.clone();
            b.iter(|| {
            let _res = runtime::execute(k.clone());
        });
        }  
    }

    #[bench]
    fn bench_unoptimized_runtime(b: &mut Bencher) {
        // Optionally include some setup
        let mut buf = current_dir().unwrap();

        buf.push("test2.txt");
        let b2 = buf.clone();
        let filename = b2.as_path();

        let plain_text = generate::get_file_as_string(filename);
        let res = generate::compile(&plain_text, false, false);

        if let Ok(x) = res {
            let k = x.clone();
            b.iter(|| {
            let _res = runtime::execute(k.clone());
        });        
        }  
    }

    #[bench]
    fn bench_optimized_async_runtime(b: &mut Bencher) {
        // Optionally include some setup
        let mut buf = current_dir().unwrap();

        buf.push("test2.txt");
        let b2 = buf.clone();
        let filename = b2.as_path();

        let plain_text = generate::get_file_as_string(filename);
        let res = generate::compile(&plain_text, true, false);

        if let Ok(x) = res {
            let k = x.clone();
            b.iter(|| {
            let _res = runtime::execute_async(k.clone());
        });
        }  
    }

    #[bench]
    fn bench_unoptimized_async_runtime(b: &mut Bencher) {
        // Optionally include some setup
        let mut buf = current_dir().unwrap();

        buf.push("test2.txt");
        let b2 = buf.clone();
        let filename = b2.as_path();

        let plain_text = generate::get_file_as_string(filename);
        let res = generate::compile(&plain_text, false, false);

        if let Ok(x) = res {
            let k = x.clone();
            b.iter(|| {
            let _res = runtime::execute_async(k.clone());
        });        
        }  
    }
}

