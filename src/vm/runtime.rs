use std::path::Path;
use std::fs::*;
use std::io::Read;
use vm::helpers::*;
use std::thread;
//use std::time;

pub fn read_file_to_byte_array(path: &Path) -> Result<Vec<u8>, String> {
    let mut buf : Vec<u8> = Vec::new();
    
    if let Ok(mut f) = File::open(path) {
        if let Ok(_x) = f.read_to_end(&mut buf) {
            return Ok(buf);
        }
    }

    Err("Something at reading bytecode file went wrong".to_owned())
}

//needed because, cant implement copy trait for Type so [Type::UnEvaluated; 256] not working
fn initialize_registers() -> Vec<Type> {
     let mut registers: Vec<Type> = Vec::new();

     for _i in 0..(Registers::RP as usize +1) {
         registers.push(Type::UnEvaluated);
     }

     registers
}

#[allow(dead_code)]
fn initialize_registers_async() -> Wav<Type> {
     let mut registers: Wav<Type> = Wav::new();

     for _i in 0..(Registers::RP as usize +1) {
         registers.push(Type::UnEvaluated);
     }

     registers
}

#[allow(dead_code)]
pub fn execute_async(byte_code_init: Vec<u8>) -> Result<String, String> {
    let mut stack : Vec<Type> = Vec::with_capacity(64);  //Should be enough Stackspace

    let mut byte_code = Wav::new();
    for b in byte_code_init {
        byte_code.push(b);
    }

    let registers = initialize_registers_async();

    let mut const_pool: Wav<Type> = Wav::new();

    let mut count = 0;

    let mut leagal_fin = false;

'run: while count < byte_code.len() {
        let inst = byte_code.get(count);

        {
            let mut rp = registers.get(Registers::RP as usize);
            if !rp.is_same_as(&Type::UnEvaluated) {
                println!("Ergebnis: \x1B[36m{}\x1B[0m", rp.to_string());
                *rp = Type::UnEvaluated;
            }
        }

         if *inst == Instructions::PUSH as u8 {
                count = count +1;
                let typ = byte_code.get(count);
                
                
                    if *typ == TypeBytes::BooleanType as u8  {

                        count = count +1;
                        match interprete_boolean(*byte_code.get(count)) {
                            Ok(x) => stack.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    else if *typ == TypeBytes::NumberType as u8 {
                        
                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(*byte_code.get(count));
                        }                       

                        match interprete_number(u8_vec) {
                            Ok(x) => stack.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    else if *typ == TypeBytes::StringType as u8 {

                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(*byte_code.get(count));
                        }
                        let bytes = u8_array_to_u64(u8_vec);

                        u8_vec= Vec::new();

                        for _i in 0..bytes {
                            count = count +1;
                            u8_vec.push(*byte_code.get(count));
                        }

                        match interprete_string(u8_vec) {
                            Ok(x) => stack.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    else if *typ == TypeBytes::UnEvaluated as u8 {

                        return Err(format!("Error: Unevaluated Type: {}", typ))
                    }

                    else{
                        return Err(format!("Error: Unknown Type specifier: {}", typ))
                    } 
                                
            }            

            else if *inst == Instructions::MOV as u8 {               
                count = count +1;
                let from = *byte_code.get(count) as usize;
                count = count +1;
                let to = *byte_code.get(count) as usize;

                if !registers.contains(from) {
                    return Err(format!("Error MOV from unknown Register: {}", from))
                }
                if !registers.contains(to) {
                    return Err(format!("Error MOV to unknown Register: {}", to))
                }
                
                *registers.get(to) = (*registers.get(from)).clone();
               
            }

            else if *inst == Instructions::POP as u8 {               
                count = count +1;
                let to = *byte_code.get(count) as usize;

                if !registers.contains(to) {
                    return Err(format!("Error POP to unknown Register: {}", to))
                }

                match stack.pop() {
                    Some(x) => *registers.get(to) = x,                    
                    None    =>  return Err(format!("Error Stack Empty at POP to: {}", to))
                }     
            }

            else if *inst == Instructions::Load as u8 {
                count = count +1;
                let bytes = vec![*byte_code.get(count),  *byte_code.get(count+1)];
                count = count +1;
                count = count +1;
                let register = *byte_code.get(count) as usize;

                let const_adress =  u8_array_to_u16(bytes) as usize;

                *registers.get(register) = (*const_pool.get(const_adress)).clone();
            }

            //Async Load
            else if *inst == Instructions::LoadA as u8 {
                let c = count;
                count = count+3;
                
                let b = byte_code.clone();
                let r = registers.clone();
                let cp = const_pool.clone();        

                thread::spawn(move || {
                    let const_bytes = vec![*b.get(c+1), *b.get(c+2)];
                    let register = *b.get(c+3) as usize;

                    let const_adress =  u8_array_to_u16(const_bytes) as usize;

                    *r.get(register) = (*cp.get(const_adress)).clone();
                }); 
            }

            else if *inst == Instructions::Add as u8 {
                count = count +1;
                let one = *byte_code.get(count) as usize;
                count = count +1;
                let two = *byte_code.get(count) as usize;

                let v1: Type;
                let v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = (*registers.get(one)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Add".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = (*registers.get(two)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Add".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(x), Type::BooleanType(y)) => stack.push(Type::BooleanType(y^x)),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 + n1;
                        stack.push(Type::NumberType(temp));  
                    }                                  
                    
                    (Type::StringType(x), Type::StringType(y)) => stack.push(Type::StringType(y + &x)),
                
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Unevaluated Types at Add".to_owned()),
                    
                    _ => return Err("Impossible Error at Add".to_owned()),
                }
            }        

            else if *inst == Instructions::Subt as u8 {
                count = count +1;
                let one = *byte_code.get(count) as usize;
                count = count +1;
                let two = *byte_code.get(count) as usize;

                let v1: Type;
                let v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = (*registers.get(one)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Subt".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = (*registers.get(two)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Subt".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(_x), Type::BooleanType(_y)) => return Err("Cant substract Booleans".to_owned()),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 - n1;
                        stack.push(Type::NumberType(temp));
                    }
                    
                    (Type::StringType(_x), Type::StringType(_y)) => return Err("Error: Cant substract Strings".to_owned()),
                    
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Error: Unevaluated Types at Subt".to_owned()),
                    _ => return Err("Impossible Error at Subt".to_owned()),
                }              
            }

            else if *inst == Instructions::Multiply as u8 {
                count = count +1;
                let one = *byte_code.get(count) as usize;
                count = count +1;
                let two = *byte_code.get(count) as usize;

                let v1: Type;
                let v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = (*registers.get(one)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Multiply".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = (*registers.get(two)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Multiply".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(x), Type::BooleanType(y)) => stack.push(Type::BooleanType(y&&x)),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 * n1;
                        stack.push(Type::NumberType(temp));                            
                    }

                    (Type::StringType(_x), Type::StringType(_y)) => return Err("Cant multiply Strings".to_owned()),
                    
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Unevaluated Types at Multiply".to_owned()),
                    _ => return Err("Impossible Error at Multiply".to_owned()),
                }
            }

            else if *inst == Instructions::Divide as u8 {
                count = count +1;
                let one = *byte_code.get(count) as usize;
                count = count +1;
                let two = *byte_code.get(count) as usize;

                let v1: Type;
                let v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = (*registers.get(one)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Divide".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = (*registers.get(two)).clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Divide".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(_x), Type::BooleanType(_y)) => return Err("Error: Cant divide Booleans".to_owned()),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 / n1;
                        stack.push(Type::NumberType(temp));  
                    }                                
                    
                    (Type::StringType(_x), Type::StringType(_y)) => return Err("Cant divide Strings".to_owned()),
                    
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Unevaluated Types at Divide".to_owned()),
                    _ => return Err("Impossible Error at Divide".to_owned()),
                }                
            }            

            else if *inst == Instructions::NOP as u8 {}

            else if *inst == Instructions::EXIT as u8 {
                leagal_fin = true;
                break 'run;
            }


            else if *inst == Instructions::SETCONST as u8 {
                count = count +1;
                let typ = *byte_code.get(count);
                
                match typ {
                    typ if typ == TypeBytes::BooleanType as u8 => {

                        count = count +1;
                        match interprete_boolean(*byte_code.get(count)) {
                            Ok(x) => const_pool.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::NumberType as u8 => {
                        
                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(*byte_code.get(count));
                        }                       

                        match interprete_number(u8_vec) {
                            Ok(x) => const_pool.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::StringType as u8 => {

                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(*byte_code.get(count));
                        }
                        let bytes = u8_array_to_u64(u8_vec);

                        u8_vec= Vec::new();

                        for _i in 0..bytes {
                            count = count +1;
                            u8_vec.push(*byte_code.get(count));
                        }

                        match interprete_string(u8_vec) {
                            Ok(x) => const_pool.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::UnEvaluated as u8 => {

                        return Err(format!("Error: Unevaluated Type: {}", typ))
                    }

                    _ => return Err(format!("Error: Unknown Type specifier: {}", typ))
                }
            }

            else {
                return Err(format!("Unknown Instruction: {}", inst))
            }
        

        count = count +1;
        //thread::sleep(time::Duration::from_nanos(SLEEP));
    }


    {
        let  rp = registers.get(Registers::RP as usize);
        if !rp.is_same_as(&Type::UnEvaluated) && !leagal_fin {
            println!("Runtime Error: \x1B[33m{}\x1B[0m", rp.to_string());

            return Err(format!("File ended before Exit code"))
        }
    }
    

    Ok(String::from("finished with Exit code \x1B[32m0x00\x1B[0m"))
}

pub fn execute(byte_code: Vec<u8>) -> Result<String, String> {
    let mut stack : Vec<Type> = Vec::with_capacity(64);  //Should be enough Stackspace

    let mut registers = initialize_registers();

    let mut const_pool: Vec<Type> = Vec::new();

    let mut count = 0;

    let mut leagal_fin = false;

'run: while count < byte_code.len() {
        let inst = byte_code[count];

        if !registers[Registers::RP as usize].is_same_as(&Type::UnEvaluated) {
            println!("Ergebnis: \x1B[36m{}\x1B[0m", registers[Registers::RP as usize].to_string());
            registers[Registers::RP as usize] = Type::UnEvaluated;
        }
        

        match inst {
            inst if inst == Instructions::PUSH as u8 => {
                count = count +1;
                let typ = byte_code[count];
                
                match typ {
                    typ if typ == TypeBytes::BooleanType as u8 => {

                        count = count +1;
                        match interprete_boolean(byte_code[count]) {
                            Ok(x) => stack.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::NumberType as u8 => {
                        
                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(byte_code[count]);
                        }                       

                        match interprete_number(u8_vec) {
                            Ok(x) => stack.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::StringType as u8 => {

                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(byte_code[count]);
                        }
                        let bytes = u8_array_to_u64(u8_vec);

                        u8_vec= Vec::new();

                        for _i in 0..bytes {
                            count = count +1;
                            u8_vec.push(byte_code[count]);
                        }

                        match interprete_string(u8_vec) {
                            Ok(x) => stack.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::UnEvaluated as u8 => {

                        return Err(format!("Error: Unevaluated Type: {}", typ))
                    }

                    _ => return Err(format!("Error: Unknown Type specifier: {}", typ))
                }                
            }            

            inst if inst == Instructions::MOV as u8 => {               
                count = count +1;
                let from = byte_code[count] as usize;
                count = count +1;
                let to = byte_code[count] as usize;

                if let None = registers.get(from) {
                    return Err(format!("Error MOV from unknown Register: {}", from))
                }
                if let None = registers.get(to) {
                    return Err(format!("Error MOV to unknown Register: {}", to))
                }
                
                registers[to] = registers[from].clone();
               
            }

            inst if inst == Instructions::POP as u8 => {               
                count = count +1;
                let to = byte_code[count] as usize;

                if let None = registers.get(to) {
                    return Err(format!("Error POP to unknown Register: {}", to))
                }

                match stack.pop() {
                    Some(x) =>  registers[to] = x,                    
                    None    =>  return Err(format!("Error Stack Empty at POP to: {}", to))
                }     
            }

            inst if inst == Instructions::Load as u8 => {
                count = count +1;
                let mut bytes = vec![byte_code[count],  byte_code[count+1]];
                count = count +1;
                count = count +1;
                let register = byte_code[count] as usize;

                let const_adress =  u8_array_to_u16(bytes) as usize;

                registers[register] = const_pool[const_adress].clone();
            }

            //Async Load
            inst if inst == Instructions::LoadA as u8 => {
                count = count +1;
                let mut bytes = vec![byte_code[count],  byte_code[count+1]];
                count = count +1;
                count = count +1;
                let register = byte_code[count] as usize;

                let const_adress =  u8_array_to_u16(bytes) as usize;

                registers[register] = const_pool[const_adress].clone();
            }

            inst if inst == Instructions::Add as u8 => {
                count = count +1;
                let one = byte_code[count] as usize;
                count = count +1;
                let two = byte_code[count] as usize;

                let mut v1: Type;
                let mut v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = registers[one].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Add".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = registers[two].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Add".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(x), Type::BooleanType(y)) => stack.push(Type::BooleanType(y^x)),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 + n1;
                        stack.push(Type::NumberType(temp));  
                    }                                  
                    
                    (Type::StringType(x), Type::StringType(y)) => stack.push(Type::StringType(y + &x)),
                
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Unevaluated Types at Add".to_owned()),
                    
                    _ => return Err("Impossible Error at Add".to_owned()),
                }
            }        

            inst if inst == Instructions::Subt as u8 => {
                count = count +1;
                let one = byte_code[count] as usize;
                count = count +1;
                let two = byte_code[count] as usize;

                let mut v1: Type;
                let mut v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = registers[one].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Subt".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = registers[two].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Subt".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(_x), Type::BooleanType(_y)) => return Err("Cant substract Booleans".to_owned()),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 - n1;
                        stack.push(Type::NumberType(temp));
                    }
                    
                    (Type::StringType(_x), Type::StringType(_y)) => return Err("Error: Cant substract Strings".to_owned()),
                    
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Error: Unevaluated Types at Subt".to_owned()),
                    _ => return Err("Impossible Error at Subt".to_owned()),
                }              
            }

            inst if inst == Instructions::Multiply as u8 => {
                count = count +1;
                let one = byte_code[count] as usize;
                count = count +1;
                let two = byte_code[count] as usize;

                let mut v1: Type;
                let mut v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = registers[one].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Multiply".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = registers[two].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Multiply".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(x), Type::BooleanType(y)) => stack.push(Type::BooleanType(y&&x)),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 * n1;
                        stack.push(Type::NumberType(temp));                            
                    }

                    (Type::StringType(_x), Type::StringType(_y)) => return Err("Cant multiply Strings".to_owned()),
                    
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Unevaluated Types at Multiply".to_owned()),
                    _ => return Err("Impossible Error at Multiply".to_owned()),
                }
            }

            inst if inst == Instructions::Divide as u8 => {
                count = count +1;
                let one = byte_code[count] as usize;
                count = count +1;
                let two = byte_code[count] as usize;

                let mut v1: Type;
                let mut v2: Type;

                if one != (Registers::STACK as usize) {
                    v1 = registers[one].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v1 = v,
                        None    => return Err("Stack ended before Divide".to_owned()),
                    }
                }

                if two != (Registers::STACK as usize) {
                    v2 = registers[two].clone();
                } 
                else {
                    match stack.pop() {
                        Some(v) => v2 = v,
                        None    => return Err("Stack ended before Divide".to_owned()),
                    }
                }


                match (v1, v2) {
                    (Type::BooleanType(_x), Type::BooleanType(_y)) => return Err("Error: Cant divide Booleans".to_owned()),

                    (Type::NumberType(n1), Type::NumberType(n2)) => {
                        let temp = n2 / n1;
                        stack.push(Type::NumberType(temp));  
                    }                                
                    
                    (Type::StringType(_x), Type::StringType(_y)) => return Err("Cant divide Strings".to_owned()),
                    
                    (Type::UnEvaluated, Type::UnEvaluated) => return Err("Unevaluated Types at Divide".to_owned()),
                    _ => return Err("Impossible Error at Divide".to_owned()),
                }                
            }            

            inst if inst == Instructions::NOP as u8 => {}

            inst if inst == Instructions::EXIT as u8 => {
                leagal_fin = true;
                break 'run;
            }


            inst if inst == Instructions::SETCONST as u8 => {
                count = count +1;
                let typ = byte_code[count];
                
                match typ {
                    typ if typ == TypeBytes::BooleanType as u8 => {

                        count = count +1;
                        match interprete_boolean(byte_code[count]) {
                            Ok(x) => const_pool.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::NumberType as u8 => {
                        
                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(byte_code[count]);
                        }                       

                        match interprete_number(u8_vec) {
                            Ok(x) => const_pool.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::StringType as u8 => {

                        let mut u8_vec : Vec<u8> = Vec::new();
                        for _i in 0..8 {
                            count = count+1;
                            u8_vec.push(byte_code[count]);
                        }
                        let bytes = u8_array_to_u64(u8_vec);

                        u8_vec= Vec::new();

                        for _i in 0..bytes {
                            count = count +1;
                            u8_vec.push(byte_code[count]);
                        }

                        match interprete_string(u8_vec) {
                            Ok(x) => const_pool.push(x),
                            Err(x)  => return Err(x),
                        }
                    }

                    typ if typ == TypeBytes::UnEvaluated as u8 => {

                        return Err(format!("Error: Unevaluated Type: {}", typ))
                    }

                    _ => return Err(format!("Error: Unknown Type specifier: {}", typ))
                }
            }

            _ => return Err(format!("Unknown Instruction: {}", inst)),
        }

        count = count +1;
    }


    {
        
        if  !registers[Registers::RP as usize].is_same_as(&Type::UnEvaluated) && !leagal_fin {
            println!("Runtime Error: \x1B[33m{}\x1B[0m", registers[Registers::RP as usize].to_string());

            return Err(format!("File ended before Exit code"))
        }
    }
    

    Ok(String::from("finished with Exit code \x1B[32m0x00\x1B[0m"))
}

