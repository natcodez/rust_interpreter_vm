#![feature(test)]

use vm::*;
use std::env::current_dir;

mod vm;

fn main() {
    let mut optimize_flag = false;
    let mut from_asm_flag = false;
    let mut to_asm_flag = false;
    
    //if wanted: change filename here:
    let code_file = "test2.txt";

    //check for flags
    for flag in std::env::args() {
        //println!("Flag: {}",flag );
        if flag == "optimize" {
            optimize_flag = true;
            println!("optimize: on");
        }
        else if flag == "from_asm" {
            from_asm_flag = true;
            println!("from_asm: on");
        }
        else if flag == "to_asm" {
            to_asm_flag = true;
            println!("to_asm: on");
        }
        //else if flag != " " {panic!("unknown argument")}
    }    


    //Get working directory and build filepath
    let mut buf = current_dir().unwrap();

    if from_asm_flag {        
        buf.push("test.dasm"); 
    }
    else {
        buf.push(code_file);
    }

    //buf.pop();
    
    let b2 = buf.clone();
    let filename = b2.as_path();


    //Read file
    let plain_text = generate::get_file_as_string(filename);

    //compile
    let res = generate::compile(&plain_text, optimize_flag, from_asm_flag);


    //write to file and run
    match res {
        Ok(x) => {
            if to_asm_flag {
                buf.pop();
                buf.push("test.dasm");
                {
                    let filename = buf.as_path();
                    match generate::write_to_file(filename, x.clone(), true) {
                        Ok(x) => {
                            println!("");
                            println!("{}", x);
                            println!("");
                        }
                        Err(x) => println!("\x1B[31m{}\x1B[0m", x)
                    }
                }
            }            

            buf.pop();
            buf.push("test.sel");
            let filename = buf.as_path();
            match generate::write_to_file(filename, x, false) {
                Ok(x) => {
                    println!("");
                    println!("{}", x);
                    println!("");
                    if let Ok(vec) = runtime::read_file_to_byte_array(filename) {
                        match runtime::execute(vec) {
                            Ok(x) => println!("{}", x),
                            Err(x) => println!("\x1B[31m{}\x1B[0m", x),
                        }
                    }
                    else {
                        println!("Error: Reading byte code gone wrong");
                    }

                }
                Err(x) => println!("\x1B[31m{}\x1B[0m", x)
            }
        }
        Err(x) => {println!("\x1B[31m{}\x1B[0m", x)}
    }
}
